import React, {useState} from 'react';
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  IconButton,
  Button,
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemText, Divider
} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import {navItems} from "../config/constants";
import CloseIcon from "@mui/icons-material/Close";

interface Props {
  window?: () => Window
}

const drawerWidth = 240

function Navbar({window}: Props) {
  const [mobileOpen, setMobileOpen] = useState(false)

  const handleDrawerToggle = () => {
    setMobileOpen(prevState => !prevState)
  }

  const container = window !== undefined ? () => window().document.body : undefined;

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{textAlign: 'center'}}>
      <Box sx={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', px: '20px'}}>
        <Typography variant="h6" sx={{my: 2}}>
          MUI
        </Typography>
        <CloseIcon/>
      </Box>
      <Divider/>
      <List>
        {navItems().map((item) => (
          <ListItem key={item.route} disablePadding>
            <ListItemButton sx={{textAlign: 'center'}}>
              <ListItemText primary={item.label}/>
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <Box height={'10vh'} sx={{display: 'flex'}}>
      <AppBar component='nav'>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{mr: 2, display: {sm: 'none'}}}
          >
            <MenuIcon/>
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{flexGrow: 1, display: {xs: 'none', sm: 'block'}}}
          >
            MUI
          </Typography>
          <Box sx={{display: {xs: 'none', sm: 'block'}}}>
            {navItems().map((item) => (
              <Button key={item.route} sx={{color: '#fff'}}>
                {item.label}
              </Button>
            ))}
          </Box>
        </Toolbar>
      </AppBar>
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true,
        }}
        sx={{
          display: {xs: 'block', sm: 'none'},
          '& .MuiDrawer-paper': {boxSizing: 'border-box', width: '100%'},
        }}
      >
        {drawer}
      </Drawer>
    </Box>
  )
    ;
}

export default Navbar;