import React from 'react';
import {Box, Button, ButtonGroup, Typography} from "@mui/material";
import {format} from 'date-fns'
import TelegramIcon from "@mui/icons-material/Telegram";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";

function Footer(): JSX.Element {
  return (
    <Box paddingY={'20px'}
         sx={{
           display: 'flex',
           justifyContent: 'space-between',
           alignItems: 'center',
           backgroundColor: 'primary.main',
           color: 'white'
         }}>
      <Typography>
        {format(new Date(), 'yyyy')} All rights reserved
      </Typography>
      <ButtonGroup sx={{display: 'flex', gap: '15px'}}>
        <TelegramIcon sx={{cursor: 'pointer'}}/>
        <InstagramIcon sx={{cursor: 'pointer'}}/>
        <YouTubeIcon sx={{cursor: 'pointer'}}/>
      </ButtonGroup>
    </Box>
  );
}

export default Footer;